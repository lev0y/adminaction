<?php 
namespace AHeadWorks\AdminAction\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

	const XML_PATH_ADMINACTION = 'adminaction/';

	public $authSession;
	public $logfactory;
	public $objectManager;
	public $request;
	public $pageTitle;
	public $registry;
	public $_log;
	public $scopeConfig;
	public $urlInterface;
	public $filter;
	public $collectionfactory;
	public $remoteAddress;

	public function __construct(
		\Magento\Framework\App\Helper\Context $context,
		\AHeadWorks\AdminAction\Model\LogFactory $logfactory,
		\AHeadWorks\AdminAction\Model\ResourceModel\Log\CollectionFactory $collectionfactory,
		\Magento\Backend\Model\Auth\Session $authSession,
		\Magento\Framework\ObjectManagerInterface $objectManager,
		\Magento\Framework\App\Request\Http $request,
		\Magento\Framework\View\Page\Title $pageTitle,
		\Magento\Framework\Registry $registry,
		\Magento\Framework\UrlInterface $urlInterface,
		\Magento\Ui\Component\MassAction\Filter $filter,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress
	)
	{
		$this->logfactory = $logfactory;
		$this->authSession = $authSession;
  		$this->objectManager = $objectManager;
  		$this->request = $request;
  		$this->pageTitle = $pageTitle;
		$this->registry = $registry; 
		$this->urlInterface = $urlInterface;
		$this->filter = $filter;
		$this->collectionfactory = $collectionfactory;
		$this->scopeConfig = $scopeConfig;
		$this->remoteAddress = $remoteAddress;
	}

	public function isLoggedIn()
	{
		return $this->authSession->isLoggedIn();
	}

	public function createLog()
	{
		$this->_log = $this->objectManager->create('AHeadWorks\AdminAction\Model\Log');
		$this->_log->setUsername($this->authSession->getUser()->getUsername());
		$this->_log->setAdminRole($this->authSession->getUser()->getRole()->getRoleName());
		$this->_log->setIpAddress($this->remoteAddress->getRemoteAddress());
		$this->_log->setPageUrl($this->urlInterface->getCurrentUrl());
	}

	public function getPageTitle()
	{
		return $this->pageTitle->getShort();
	}

	public function saveLog()
	{
		$this->_log->setDateTime(date("d-m-Y H:i:s"));
		$this->_log->save();
	}

	public function setOperation($operation, $_item = null)
	{
		switch ($operation) 
		{
			case 'Created':
				$pageTitle = $this->registry->registry('current_product')->getName();
				break;
			case 'Viewed':
				$pageTitle = $this->getPageTitle();//'<a href="'.$this->urlInterface->getCurrentUrl().'">'.$this->getPageTitle().'</a>';
				break;
			case 'Updated':
				$pageTitle = $this->registry->registry('current_product')->getSku();
				break;
			case 'Deleted':
				$pageTitle = $_item;
				break;//*/
			default:
				$pageTitle = 'Unknown';
				break;
		}
		
		$this->_log->setPageTitle($pageTitle);
		$this->_log->setOperation($operation);
	}

	public function getConfigValue($field, $storeId = null)
	{
		return $this->scopeConfig->getValue(
			$field, ScopeInterface::SCOPE_STORE, $storeId
		);
	}

	public function getGeneralConfig($code, $storeId = null)
	{

		return $this->getConfigValue(self::XML_PATH_ADMINACTION .'general/'. $code, $storeId);
	}

	public function isEnable()
	{
		if ($this->isLoggedIn() && $this->getGeneralConfig('enable')) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}