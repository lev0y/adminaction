<?php 
namespace AHeadWorks\AdminAction\Observer;

use AHeadWorks\AdminAction\Helper\Data as Helper;

class DeleteBefore implements \Magento\Framework\Event\ObserverInterface
{
	public $log;

	public function __construct(
		Helper $log) 
	{
		$this->log = $log;

    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
    	if ($this->log->isEnable()) 
    	{
            $item = $observer->getData('object')->getSku();
            $this->log->createLog();
            $this->log->setOperation('Deleted', $item);
            $this->log->saveLog();
    	}
    }
}