<?php 
namespace AHeadWorks\AdminAction\Observer;

use AHeadWorks\AdminAction\Helper\Data as Helper;

class PostDispatch implements \Magento\Framework\Event\ObserverInterface
{
	public $log;

	public function __construct(
		Helper $log) 
	{
		$this->log = $log;

    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
    	if ($this->log->isEnable() && $this->log->getPageTitle()) 
    	{
    		$this->log->createLog();
			$this->log->setOperation('Viewed');
			$this->log->saveLog();
    	}
    }
}