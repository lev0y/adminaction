<?php
namespace AHeadWorks\AdminAction\Observer;

use AHeadWorks\AdminAction\Helper\Data as Helper;

class SaveBefore implements \Magento\Framework\Event\ObserverInterface
{
	public $log;

	public function __construct(
		Helper $log) 
	{
		$this->log = $log;
    }

	public function execute(\Magento\Framework\Event\Observer $observer)
	{	
		/*$f = fopen('/tmp/saveBefore.txt', 'w+');
		$product = $observer->getData('product');
		$data = get_class_methods($observer);
        foreach ($data as $key) {
            $w = fwrite($f, $key . "\n");
        }
        if ($product!=null) {
        	$w = fwrite($f, 'Product ID = ' . $product->getId());
        }
        else
        {
        	$w = fwrite($f, 'Sorry(');	
        }
		*/
	
    	if ($this->log->isEnable()) 
    	{
    		$this->log->createLog();
    		$product = $observer->getData('product');
    		$productId = $product->getId();
    		$productName = $product->getName();
    		if (!$productId) 
    		{
        		$operation = 'Created';
	        }
	        else
	        {
	        	$operation = 'Updated';	
	        }
			$this->log->setOperation($operation);
			$this->log->saveLog();



			/*$object = $observer->getData('product');
			$t = get_class_methods($object);
			$f = fopen('/tmp/saveBefore.txt', 'w+');
		//	$w = fwrite($f, $t . "\n");
			foreach ($t as $key) {
            	$w = fwrite($f, $key . "\n");
       		}
       		$w = fwrite($f, 'Product name - ' . $object->getName() . "\n");
			/*if ($object!=null) {
        		$w = fwrite($f,  . "\n");
	        }
	        else
	        {
	        	$w = fwrite($f, 'Sorry(');	
	        }*/
			
		//$product = $observer->getData('product');
		/*$data = get_class_methods($object);
        foreach ($data as $key) {
            $w = fwrite($f, $key . "\n");
        }//*/
    	}
    	
	}
}