<?php
namespace AHeadWorks\AdminAction\Block\Adminhtml;

class Log extends \Magento\Backend\Block\Widget\Grid\Container
{

	protected function _construct()
	{
		$this->_controller = 'adminhtml_index';
		$this->_blockGroup = 'AHeadWorks_AdminAction';
		$this->_headerText = __('Logs');
		parent::_construct();
	}
}