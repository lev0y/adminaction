<?php 
namespace AHeadWorks\AdminAction\Block\Adminhtml\System\Config;

class GridConfig extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
    protected function _prepareToRender()
    {
        $this->addColumn('role_name', ['label' => __('Role Name')]);
        $this->addColumn('state', ['label' => __('State')]);
        $this->_addAfter = false;   
      
        $methods = get_class_methods($this);
        $f = fopen('/tmp/test.txt', 'w+');
        foreach ($methods as $method) {
            $w = fwrite($f, $method . "\n");
        }
    }
}