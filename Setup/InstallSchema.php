<?php
namespace AHeadWorks\AdminAction\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
	public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();
		if (!$installer->tableExists('aheadworks_adminaction_logs')) {
			$table = $installer->getConnection()->newTable(
				$installer->getTable('aheadworks_adminaction_logs')
			)
				->addColumn(
					'id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					[
						'identity' => true,
						'nullable' => false,
						'primary'  => true,
						'unsigned' => true,
					],
					'ID'
				)
				->addColumn(
					'username',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					['nullable => false'],
					'Username'
				)
				->addColumn(
					'admin_role',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					[],
					'Admin role'
				)
				->addColumn(
					'ip_address',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					16,
					['unsigned' => true,],
					'IP address'
				)
				->addColumn(
					'page_title',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					null,
					[],
					'Page Title'
				)
				->addColumn(
					'page_url',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					null,
					[],
					'Page URL'
				)
				->addColumn(
					'operation',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					[],
					'Operation'
				)
				->addColumn(
					'date_time',
					\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
					null,
					['nullable' => false,],
					'Date/Time'
				);
			$installer->getConnection()->createTable($table);
		}

		if (!$installer->tableExists('aheadworks_adminaction_role_logging')) {
			$table = $installer->getConnection()->newTable(
				$installer->getTable('aheadworks_adminaction_role_logging')
			)
				->addColumn(
					'role_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					10,
					[
						'identity' => true,
						'nullable' => false,
						'primary'  => true,
						'unsigned' => true,
					],
					'Role ID'
				)
				->addColumn(	
					'role_name',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					50,
					[],
					'Role name'
				)
				->addColumn(
					'state',
					\Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
					null,
					['default' => true],
					'State'
				);
			$installer->getConnection()->createTable($table);
		}
		$installer->endSetup();
	}
}