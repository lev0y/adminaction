<?php 
namespace AHeadWorks\AdminAction\Setup; 

use Magento\Framework\Setup\InstallDataInterface; 
use Magento\Framework\Setup\ModuleContextInterface; 
use Magento\Framework\Setup\ModuleDataSetupInterface; 

class InstallData implements InstallDataInterface 
{ 
	public function install(
		ModuleDataSetupInterface $setup, 
		ModuleContextInterface $context) 
	{ 
		$setup->getConnection()->query("INSERT INTO aheadworks_adminaction_role_logging (role_id, role_name) SELECT role_id, role_name FROM authorization_role WHERE parent_id=0;"); 
	} 
}