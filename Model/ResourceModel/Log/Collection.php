<?php
namespace AHeadWorks\AdminAction\Model\ResourceModel\Log;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'aheadworks_adminaction_logs_collection';
	protected $_eventObject = 'logs_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('AHeadWorks\AdminAction\Model\Log', 'AHeadWorks\AdminAction\Model\ResourceModel\Log');
	}

}