<?php
namespace AHeadWorks\AdminAction\Model\ResourceModel\RoleState;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'role_id';
	protected $_eventPrefix = 'aheadworks_adminaction_rolestate_collection';
	protected $_eventObject = 'rolestate_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('AHeadWorks\AdminAction\Model\RoleState', 'AHeadWorks\AdminAction\Model\ResourceModel\RoleState');
	}

}