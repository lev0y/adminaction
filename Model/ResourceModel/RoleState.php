<?php
namespace AHeadWorks\AdminAction\Model\ResourceModel;

class RoleState extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}
	
	protected function _construct()
	{
		$this->_init('aheadworks_adminaction_rolestate', 'role_id');
	}
	
}