<?php 
namespace AHeadWorks\AdminAction\Model\Config\Backend;

class RoleState extends \Magento\Framework\App\Config\Value
{

    //public $objectManager;

    public function __construct()//\Magento\Framework\ObjectManagerInterface $objectManager)
    {
      //  $this->objectManager = $objectManager;
    }

    /**
     * Process data after load
     *
     * @return void
     */
    protected function _afterLoad()
    {
        $value = $this->getValue();
        $arr = unserialize($value);

        $this->setValue($arr);
    }

    /**
     * Prepare data before save
     *
     * @return void
     */
    public function beforeSave()
    {
        $value = $this->getValue();
        unset($value['__empty']);
        $arr = serialize($value);

        $this->setValue($arr);
    }
}