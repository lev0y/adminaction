<?php
namespace AHeadWorks\AdminAction\Model;

class Log 
	extends \Magento\Framework\Model\AbstractModel 
	implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'aheadworks_adminaction_log';

	protected $_cacheTag = 'aheadworks_adminaction_log';

	protected $_eventPrefix = 'aheadworks_adminaction_log';

	protected function _construct()
	{
		$this->_init('AHeadWorks\AdminAction\Model\ResourceModel\Log');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}