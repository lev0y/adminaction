<?php
namespace AHeadWorks\AdminAction\Model;

class RoleState 
	extends \Magento\Framework\Model\AbstractModel 
	implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'aheadworks_adminaction_rolestate';

	protected $_cacheTag = 'aheadworks_adminaction_rolestate';

	protected $_eventPrefix = 'aheadworks_adminaction_rolestate';

	protected function _construct()
	{
		$this->_init('AHeadWorks\AdminAction\Model\ResourceModel\RoleState');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}