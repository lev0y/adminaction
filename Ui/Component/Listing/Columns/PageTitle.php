<?php

namespace AHeadWorks\AdminAction\Ui\Component\Listing\Columns;

use Magento\Framework\Escaper;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;

class PageTitle extends Column{
    
    protected $escaper;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Escaper $escaper,
        array $components = [],
        array $data = []
    ) {
        $this->escaper = $escaper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource){
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                $html = '<a href="https://www.google.by/webhp?tab=rw" target="_blank">' . __('Label'). '</a>';
                $item[$fieldName] = $this->escaper->escapeHtml($html, ['a']);
            }
        }
        ?> <script>alert("Sorry(");</script><?php  
        return $dataSource;
    }

}